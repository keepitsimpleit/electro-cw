// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron')
const path = require('path')
const fs = require('fs')

app.allowRendererProcessReuse = true

function createWindow () {
  // Create the browser window
  const mainWindow = new BrowserWindow({
    width: 1200,
    height: 600,
    autoHideMenuBar: true,
    show: false,
    title: 'Connectwise - Keep IT Simple',
    webPreferences: {
      nodeIntegration: true,
      preload: path.join('./preload.js'),
      zoomFactor: .9
    }
  })

  //Create the loading window
  const loadingWindow = new BrowserWindow({
    parent: mainWindow,
    modal: true,
    width: 420,
    height: 250,
    frame: false,
    resizable: false,
    webPreferences: {
      nodeIntegration: true,
      zoomFactor: 0.7
    }
  })

  // Add content to loading window
  loadingWindow.loadFile('./loading.html')

  // Run the renderer to change the titlebar
  mainWindow.loadFile('./index.html')

  // Start loading the main website
  mainWindow.webContents.loadURL('https://keepitsimple.hostedrmm.com/automate')

  // Open the DevTools.
  // mainWindow.webContents.openDevTools()

  // Prevent page title from changing
  mainWindow.on('page-title-updated', (evt) => {
    evt.preventDefault()
  })

  // Hide loadingWindow after mainWindow finishes loading
  mainWindow.webContents.on('did-finish-load', function() {
    fs.readFile('./style/darktheme.css', "utf-8", function(error, data) {
      if(!error){
        var formattedData = data.replace(/\s{2,10}/g, ' ').trim()
        mainWindow.webContents.insertCSS(formattedData)
      }
      else{
        console.log('Error reading CSS file')
      }
    })
    loadingWindow.hide()
    mainWindow.show()
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
