const customTitlebar = require('custom-electron-titlebar')

// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.

new customTitlebar.Titlebar({
    backgroundColor: customTitlebar.Color.fromHex('#000'),
})

MyTitleBar.updateTitle('Connectwise | Keep IT Simple')
console.log('!!! RAN THE RENDERER !!!')